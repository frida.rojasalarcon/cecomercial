<%-- 
    Document   : DatosUsuario
    Created on : 7/10/2021, 11:10:18 AM
    Author     : adirF
--%>
<%@include file="Header.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Temas-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="estilo.css">
    </head>
    <body>
        <div id="Contenedor">
            <div class="col-9" >
                <form class="row g-4">
                    <label for="validationDefault01" class="form-label" style="color:beige">Nombre</label>
                    <input type="text" class="form-control" id="validationDefault01" value="hola" required>

                    <label for="validationDefault02" class="form-label"style="color:beige">Apellidos</label>
                    <input type="text" class="form-control" id="validationDefault02" value="que hace" required>


                    <label for="validationDefaultUsername" class="form-label"style="color:beige">Nombre de Usuario</label>
                    <div class="input-group">
                        <span class="input-group-text" id="inputGroupPrepend2">@</span>
                        <input type="text" class="form-control" id="validationDefaultUsername"  aria-describedby="inputGroupPrepend2" required>
                    </div>


                    <label for="validationDefault03" class="form-label"style="color:beige">Contraseña</label>
                    <input type="text" class="form-control" id="validationDefault03" required>

                    <label for="validationDefault04" class="form-label"style="color:beige">Ciudad</label>
                    <select class="form-select" id="validationDefault04" required>
                        <option selected disabled value="">Choose...</option>
                        <option>...</option>
                    </select>



                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required>
                        <label class="form-check-label" for="invalidCheck2"style="color:beige">
                            Acepta Terminos y condiciones
                        </label>
                    </div>


                    <button class="btn btn-primary" type="submit"style="color:beige">Registrarme!</button>

                </form>

            </div>


        </div>

    </body>
    <footer>
        <%@include file="Footer.jsp" %>    
    </footer>

</html>
